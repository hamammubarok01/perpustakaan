@extends('layout.form')
@section('konten')
<!-- START DATA -->
<div class="my-3 p-3 bg-body rounded shadow-sm">
    <div class="row">
    @foreach ($data as $item)
    <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
    <div class="card" style="width: 100;">
                        <img src="{{ url("buku1/$item->gambar_buku")}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->nama_buku}}</h5>
                            <p class="card-text">Pengarang : {{ $item->pengarang_buku}}</p>
                            <p class="card-text">Penerbit : {{ $item->penerbit_buku}}</p>
                            <p class="card-text">Stok : {{ $item->stok_buku}}</p>
                            <a href="/meminjam/{{ $item->id}}" class="btn btn-primary">Pinjam Buku</a>
                        </div>
                    </div>
        </div>
        @endforeach
    </div>
        </body>
    </table>
</div>
@endsection
