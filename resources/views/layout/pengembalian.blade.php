@extends('layout.form')
@section('konten')
<!-- START DATA -->
<div class="my-3 p-3 bg-body rounded shadow-sm" style="min-height: 80vh;">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="col-md-1">No</th>
                <th class="col-md-4">Nama Buku</th>
                <th class="col-md-4">Pengarang Buku</th>
                <th class="col-md-2">Penerbit Buku</th>
                <th class="col-md-4">Gambar Buku</th>
                <th class="col-md-4">Tanggal Pengembalian</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->books->nama_buku}}</td>
                <td>{{ $item->books->pengarang_buku}}</td>
                <td>{{ $item->books->penerbit_buku}}</td>
                <?php $gambar=$item->books->gambar_buku ?>
                <td><img src="{{ url("buku1/$gambar")}}" width="100px"></td>
                <td>{{ $item->tanggal_pengembalian}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
