@extends('layout.form')
@section('konten')
<!-- START DATA -->
<div class="my-3 p-3 bg-body rounded shadow-sm" style="min-height: 80vh;">
    <!-- TOMBOL TAMBAH DATA -->
    <div class="pb-3">
    <a href='{{url('/buku/create')}}' class="btn btn-primary">+ Tambah Data</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="col-md-1">No</th>
                <th class="col-md-3">Nama Buku</th>
                <th class="col-md-4">Gambar</th>
                <th class="col-md-2">Pengarang</th>
                <th class="col-md-2">Penerbit</th>
                <th class="col-md-2">Stok</th>
                <th class="col-md-2">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->nama_buku}}</td>
                <td><img src="{{ url("buku1/$item->gambar_buku")}}" width="100px"></td>
                <td>{{ $item->pengarang_buku}}</td>
                <td>{{ $item->penerbit_buku}}</td>
                <td>{{ $item->stok_buku}}</td>
                <td>
                    <a href='{{ url('buku/'.$item->id.'/edit') }}' style="width: 100px" class="btn btn-warning btn-sm">Edit</a>
                    <form onSubmit="return confirm('Yakin akan menghapus data buku tersebut?') "class='d-inline' action="{{ url('buku/'.$item->id) }}"
                    method="post">
                        @csrf
                        @method('DELETE')
                        <button style="width: 100px" type="submit" name="submit" class="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
