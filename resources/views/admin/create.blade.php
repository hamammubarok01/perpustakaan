@extends('layout.form')
@section('konten')
{{-- @include('komponen.message') --}}
<!-- START FORM -->
<form action='{{url('buku')}}' method='post' enctype="multipart/form-data">
    @csrf
    <div class="my-3 p-3 bg-body rounded shadow-sm">
        <a href="{{ url('buku') }}" class="btn btn-secondary">Kembali</a>
        <div class="mb-3 row">
            <label for="nim" class="col-sm-2 col-form-label">Nama Buku</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name='namabuku' value="{{ Session::get('nama_buku') }}" id="namabuku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="nama" class="col-sm-2 col-form-label">Gambar</label>
            <div class="col-sm-10">
                <input type="file" class="form-control" name='gambar_buku' id="gambar_buku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="jurusan" class="col-sm-2 col-form-label">Pengarang</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name='pengarangbuku' value="{{ Session::get('pengarang_buku') }}" id="pengarangbuku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="jurusan" class="col-sm-2 col-form-label">Penerbit</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name='penerbitbuku' value="{{ Session::get('penerbit_buku') }}" id="penerbitbuku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="jurusan" class="col-sm-2 col-form-label">Stok</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name='stokbuku' value="{{ Session::get('stok_buku') }}" id="stokbuku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="jurusan" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10"><button type="submit" class="btn btn-primary" name="submit">SIMPAN</button></div>
        </div>
    </div>
</form>
    <!-- AKHIR FORM -->
@endsection
