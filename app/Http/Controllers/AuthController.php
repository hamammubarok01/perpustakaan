<?php

namespace App\Http\Controllers;

use App\Models\role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function autentikasi(Request $request)
    {
        Session::flash('email',$request->email);
        Session::flash('password',$request->password);

        $request->validate(
            [
                'email'=> 'required',
                'password'=> 'required',
            ]);

        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // dd(Auth::user());

        if (Auth::attempt($infologin)){
            if (Auth::user()->role_id=='1'){
                return redirect('/dashboard_admin');
            }else if (Auth::user()->role_id=='2'){
                return redirect('/dashboard_user');
            }
        }else{
            return redirect('/')->with('notvalid','email dan password yang dimasukkan tidak sesuai');
        }
    }
    public function admin()
    {
        return view('admin.dashboard');
    }

    public function mahasiswa()
    {
        return view('mahasiswa.dashboard');
    }

}
