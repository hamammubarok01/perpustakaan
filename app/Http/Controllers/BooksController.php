<?php

namespace App\Http\Controllers;

use App\Models\books;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = books::orderBy('id')->get();
        return view('admin.daftarbuku')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Session::flash('nama_buku',$request->namabuku);
        Session::flash('pengarang_buku',$request->pengarangbuku);
        Session::flash('penerbit_buku',$request->penerbitbuku);
        Session::flash('stok_buku',$request->stokbuku);
        
        $request->validate([
            'namabuku' => 'required',
            'pengarangbuku' => 'required',
            'penerbitbuku' => 'required',
            'stokbuku' => 'required',
        ],[
            'namabuku.required' => 'Nama buku wajib diisi',
            'pengarangbuku.required' => 'Pengarang buku wajib diisi',
            'penerbitbuku.required' => 'Penerbit buku wajib diisi',
            'stokbuku.required' => 'Stok buku wajib diisi',
        ]);
        
        $data = [
            'nama_buku' => $request->namabuku,
            // 'gambar_buku' => $request->gambar_buku,
            'pengarang_buku' => $request->pengarangbuku,
            'penerbit_buku' => $request->penerbitbuku,
            'stok_buku' => $request->stokbuku,
        ];
        $gambar = $request->file('gambar_buku');
        $nama_gambar = $gambar->getClientOriginalName();
        $gambar->move('buku1',$gambar->getClientOriginalName());
        $data['gambar_buku']=$nama_gambar;

        books::create($data);
        return redirect()->to('buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = books::where('id',$id)->first();
        return view('admin.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namabuku' => 'required',
            'pengarangbuku' => 'required',
            'penerbitbuku' => 'required',
            'stokbuku' => 'required',
        ],[
            'namabuku.required' => 'Nama buku wajib diisi',
            'pengarangbuku.required' => 'Pengarang buku wajib diisi',
            'penerbitbuku.required' => 'Penerbit buku wajib diisi',
            'stokbuku.required' => 'Stok buku wajib diisi',
        ]);
        
        $data = [
            'nama_buku' => $request->namabuku,
            // 'gambar_buku' => $request->gambarbuku,
            'pengarang_buku' => $request->pengarangbuku,
            'penerbit_buku' => $request->penerbitbuku,
            'stok_buku' => $request->stokbuku,
        ];
        
        if($request->gambar_buku){
            $gambar = public_path('/buku1/') . $request->gambar_old;
            unlink($gambar);
            $gambar = $request->file('gambar_buku');
            $nama_gambar = $gambar->getClientOriginalName();
            $gambar->move('buku1',$gambar->getClientOriginalName());
            $data['gambar_buku']=$nama_gambar;
        }


        books::where('id',$id)->update($data);
        return redirect()->to('buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $books = books::find($id);
        $gambar = public_path('/buku1/') . $books->gambar_buku;
        unlink($gambar); //menghapus gambar yang ada di direktori
        $books->delete(); //menghapus semua data dan juga gambar yang ada di database tidak menghapus gambar yang ada di folder direktori
        return redirect()->to('buku');
    }

    // public function user()
    // {
    //     return view('mahasiswa.daftarbuku');
    // }
}
