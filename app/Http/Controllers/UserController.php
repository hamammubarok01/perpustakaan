<?php

namespace App\Http\Controllers;

use App\Models\books;
use App\Models\User;
use App\Models\peminjaman;
use App\Models\pengembalian;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index ()
    {
        $data = books::orderBy('id')->get();
        return view('mahasiswa.daftarbuku')->with('data',$data);
    }
    public function peminjaman ()
    {
        $data = peminjaman::orderBy('id')->where('id_user', Auth::user()->role_id)->get();
        return view('mahasiswa.peminjaman')->with('data',$data);
    }
    public function pengembalian ()
    {
        $data = pengembalian::orderBy('id')->where('id_user', Auth::user()->role_id)->get();
        return view('mahasiswa.pengembalian')->with('data',$data);
    }
    public function meminjam ($id)
    {
        $data = ['id_user' => Auth::user()->role_id, 'id_books' => $id, 'tanggal_peminjaman' => Carbon::now()];
        peminjaman::create($data);

        $data = ['id_user' => Auth::user()->role_id, 'id_books' => $id, 'tanggal_pengembalian' => Carbon::now()->addDays(3)];
        pengembalian::create($data);

        $data = books::where('id',$id)->first();
        $data = [
                'nama_buku' => $data["nama_buku"],
                'pengarang_buku' => $data["pengarang_buku"],
                'gambar_buku' => $data["gambar_buku"],
                'penerbit_buku' => $data["penerbit_buku"],
                'stok_buku' => $data["stok_buku"]-1,
        ];
        books::where('id',$id)->update($data);
        return redirect('peminjaman');
    }

    public function anggota ()
    {
        $data = User::orderBy('id')->get();
        return view('admin.anggota')->with('data', $data);
    }
}

