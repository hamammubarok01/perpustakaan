<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class books extends Model
{
    use HasFactory;
    protected $fillable = ['nama_buku','gambar_buku','pengarang_buku','penerbit_buku','stok_buku'];
    // protected $table = 'books';
    public $timestamps = false;
}
