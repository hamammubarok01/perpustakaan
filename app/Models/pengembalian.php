<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengembalian extends Model
{
    use HasFactory;
    protected $table = 'pengembalian';
    protected $fillable = ['id','id_user','id_books','tanggal_pengembalian'];
    // protected $table = 'books';
    public $timestamps = false;
    public function users() {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function books() {
        return $this->belongsTo(books::class, 'id_books');
    }
}
