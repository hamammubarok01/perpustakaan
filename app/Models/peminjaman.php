<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class peminjaman extends Model
{
    use HasFactory;
    protected $table = 'peminjaman';
    protected $fillable = ['id','id_user','id_books','tanggal_peminjaman'];
    // protected $table = 'books';
    public $timestamps = false;
    public function users() {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function books() {
        return $this->belongsTo(books::class, 'id_books');
    }
}
