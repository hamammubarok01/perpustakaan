<?php

use App\Http\Middleware\user;
use App\Http\Middleware\admin;
use App\Http\Middleware\userlog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\LogoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// })->middleware('auth');

// Route::post('/login');

Route::get('/',[AuthController::class, 'login']);
Route::post('/login',[AuthController::class, 'autentikasi']);

Route::get('/dashboard_admin',[AuthController::class, 'admin']);
Route::get('/dashboard_user',[AuthController::class, 'mahasiswa']);

Route::resource('/buku', BooksController::class)->middleware('admin');
Route::get('/buku/create', [BooksController::class, 'create'])->middleware('admin');

Route::middleware('auth')->group(function (){

    Route::post('logout',LogoutController::class)->name('logout');
});

Route::get('/daftarbuku', [UserController::class,'index'])->middleware('user');
Route::get('/peminjaman', [UserController::class,'peminjaman'])->middleware('user');
Route::get('/pengembalian', [UserController::class,'pengembalian'])->middleware('user');
Route::get('/meminjam/{id}', [UserController::class,'meminjam'])->middleware('user');
Route::get('/anggota',[UserController::class, 'anggota'])->middleware('admin');;
