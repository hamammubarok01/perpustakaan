<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'hamam',
            'email' => 'admin@gmail.com',
            'role_id' => 1,
            'level' => 'Admin',
            'password' => bcrypt('admin'),
        ]);
        User::create([
            'name' => 'hamam',
            'email' => 'hamam@gmail.com',
            'role_id' => 2,
            'level' => 'Mahasiswa',
            'password' => bcrypt('hamam'),
        ]);
    }
}
